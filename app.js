// // Задание 1

// const numbers = {
//     keyin1: 1,
//     keyin2: 2,
//     keyin3: 3,
//     keyin4: 4,
//     keyin5: 5,
//     keyin6: 6,
//     keyin7: 7,
// }
// let number = Object.values(numbers);
// console.log(number);
// for (let value of number) {
//     if (value >= 3) {
//         console.log(value);
//     }
// }

// // Задание 2

// const post = {
//     author: "John", // вывести этот текст
//     postId: 23,
//     comments: [
//         {
//             userId: 10,
//             userName: "Alex",
//             text: "lorem ipsum",
//             rating: {
//                 likes: 10,
//                 dislikes: 2, // вывести это число
//             },
//         },
//         {
//             userId: 5, // вывести это число
//             userName: "Jane",
//             text: "lorem ipsum 2", // вывести этот текст
//             rating: {
//                 likes: 3,
//                 dislikes: 1,
//             },
//         },
//     ],
// };


// console.log(post.author);

// for (let i = 0; i < 1; i++) {
//     const count1 = post.comments[i].rating;
//     console.log(Object.keys(count1.dislikes).reduce(Element, count1.dislikes));
// }

// for (let g = 0; g > 1; g++) {
//     const count2 = post.comments[g];
//     console.log(count2);
//     console.log(count2.text);
// }

// // Задание 3

// const products = [
//     {
//         id: 3,
//         price: 200,
//     },
//     {
//         id: 4,
//         price: 900,
//     },
//     {
//         id: 1,
//         price: 1000,
//     },
// ];

// products.forEach(function (elem1) {
//     console.log(elem1.price - (elem1.price * 0.15));
// })

// Задание 4

const products = [
    {
        id: 3,
        price: 127,
        photos: [
            "1.jpg",
            "2.jpg",
        ],
    },
    {
        id: 5,
        price: 499,
        photos: [],
    },
    {
        id: 10,
        price: 26,
        photos: [
            "3.jpg",
        ],
    },
    {
        id: 8,
        price: 78,
    },
];

// for (let i = 0; i < products.length; i++) {
//     const element = products[i].photos;

//     if (element.length !== 0) {
//         console.log(element)
//     }
// }

for (let p = 0; p < products.length; p++) {
    const elementPrice = products[p].price;
    console.log(elementPrice);
    elementPrice.sort((a, b) => {
        return a - b;
    });
    console.log(elementPrice);
}

// Задание 5

// const en = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
// const ru = ["понедельник", "вторник", "среда", "четверг", "пятница", "суббота", "воскресенье"];
// const allArray = [...en, ...ru];
// console.log(allArray);
