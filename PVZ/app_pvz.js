const telEl = document.querySelector('.telEl');
telEl.addEventListener('focus', (e) => {
    e.target.value = "+7 ";
})

const chekBoxBtnBrandEl = document.querySelector('.chekBoxBtnBrand');
const chekBoxBtnNoBrandEl = document.querySelector('.chekBoxBtnNoBrand');
const formEl = document.querySelector('.form');
const fullAdressEl = document.querySelector('.fullAdress');
const errorEl = document.querySelector('.error');
const errorFullAdressEl = document.querySelector('.errorFullAdress');
const btnEL = document.querySelector('.btn');

chekBoxBtnNoBrandEl.addEventListener('click', function (e) {
    const target = e.target;
    if (target.checked) {
        fullAdressEl.classList.remove('noBrandAdress');
    } else {
        fullAdressEl.classList.add('noBrandAdress');
    }
});

btnEL.addEventListener('click', () => {
    if (chekBoxBtnBrandEl.checked === false && chekBoxBtnNoBrandEl.checked === false) {
        errorEl.classList.add("visible");
    } else if (chekBoxBtnBrandEl.checked || chekBoxBtnNoBrandEl.checked) {
        errorEl.classList.remove("visible");
    }
});